A subset (availability bias) of things that I have found valuable over the years on the Internet (A never completing list).

# Inspirational Video & Articles

You Can Learn Anything, Khan Academy - [youtube.com](https://youtu.be/JC82Il2cjqA)   

The Greatest Story Ever Told, Part 1 - [youtube.com](https://youtu.be/A2wG0sXbMhw)   
The Greatest Story Ever Told, Part 2 - [youtube.com](https://youtu.be/-JOXUNX66Cs)   

Steve Jobs' 2005 Stanford Commencement Address - [youtube.com](https://youtu.be/UF8uR6Z6KLc)   
A Pale Blue Dot by Carl Sagan - [youtube.com](https://youtu.be/EWPFmdAWRZ0)   

Gods and Worms by Jason Silva - [youtube.com](https://youtu.be/mJKxxaCmJLo)   
Rick and Morty and the Meaning of Life - [hackernoon.com](https://hackernoon.com/rick-and-morty-and-the-meaning-of-life-6640df17e263)  

Ashtavakra Gita, by Sandeep Maheshwari (Hindi) - [youtube.com](https://www.youtube.com/playlist?list=PLAQW7o4sNH26XaJQNi-rpfAu51TaHGByV)    

The Egg, A Short Story, from Kurzgesagt - [youtube.com](https://youtu.be/h6fcK_fRYaI)   
The Beginning & End of Humanity, from Pursuit of Wonder - [youtube.com](https://youtu.be/uHu8fEIViA8)   

Dictionary of Obscure Sorrows - [youtube.com](https://www.youtube.com/playlist?list=PLCXinA-ukN0rxft8oNal97bH3FfRzSZXZ)   
The Humans of New York, Instagram - [instagram.com](https://www.instagram.com/humansofny)   

# Interesting Resources

Two Minutes to Midnight, Aperture - [youtube.com](https://youtu.be/bsZ37kxrQYg)   
The Artificial Intelligence That Deleted A Century, by Tom Scott - [youtube.com](https://youtu.be/-JlxuQ7tPgQ)   

Jailbreaking the Simulation by George Hotz - [youtube.com](https://youtu.be/ESXOAJRdcwQ)   

Why Boredom is Good For You, by Veritasium - [youtube.com](https://www.youtube.com/watch?v=LKPwKFigF8U)   
Thinking about Attention, by CGP Grey - [youtube.com](https://youtu.be/wf2VxeIm1no)   
How to Get Your Life Back Together, by Improvement Pill - [youtube.com](https://youtu.be/vl-44jDYDJQ)

You, Version Two, from Freedom in Thought - [youtube.com](https://www.youtube.com/playlist?list=PL5faAYlGYYoGwdnSOCC2uOeJM4_cnjEME)

History of the Entire World, by Bill Wurtz - [youtube.com](https://youtu.be/xuCn8ux2gbs)   
Evolution (Mathematics of Evolution), by Justin Helps - [youtube.com](https://www.youtube.com/playlist?list=PLKortajF2dPBWMIS6KF4RLtQiG6KQrTdB)   
Memes & The Evolution of Art, from Pursuit of Wonder - [youtube.com](https://youtu.be/vWaId-BvIrA)   

This is the Scariest Graph, by Hank Green - [youtube.com](https://youtu.be/Ix2XprA6A7I)   

Steve Jobs, Think Different (Best Marketing Strategy) - [youtube.com](https://youtu.be/keCwRdbwNQY)   

Artificial Intelligence Podcast by Lex Fridman - [youtube.com](https://www.youtube.com/playlist?list=PLrAXtmErZgOdP_8GztsuKi9nrraNbKKp4)   

# Interesting Resources (Technical)

Building an 8-bit Breadboard Computer by Ben Eater - [youtube.com](https://www.youtube.com/playlist?list=PLowKtXNTBypGqImE405J2565dvjafglHU)   
Neural Network, by 3Blue1Brown - [youtube.com](https://www.youtube.com/playlist?list=PLZHQObOWTQDNU6R1_67000Dx_ZCJB-3pi)

# Free Learning Platforms on The Internet

Youtube Searchbar - [https://www.youtube.com](https://www.youtube.com)   
Wikipedia - [https://www.wikipedia.org](https://www.wikipedia.org/)

Khan Academy - [https://www.khanacademy.org](https://www.khanacademy.org/)   
MIT OpenCourseWare - [https://ocw.mit.edu](https://ocw.mit.edu)   
Edx - [https://www.edx.org](https://www.edx.org/)   
Coursera - [https://www.coursera.org](https://www.coursera.org/)   
<!--Udacity Free Courses - [https://udacity.com/courses/all](https://udacity.com/courses/all)   -->
<!--Stanford Online - [https://online.stanford.edu](https://online.stanford.edu/)   -->

# Free Learning Resources (Business & Startup)

Startup Funding Explained, in 10 minutes - [youtube.com](https://www.youtube.com/watch?v=677ZtSMr4-4)

Startup Playbook by Sam Altman - [playbook.samaltman.com](https://playbook.samaltman.com/)   
How to Start a Startup, Y Combinator at Stanford, 2015 - [youtube.com](https://www.youtube.com/playlist?list=PL5q_lef6zVkaTY_cT1k7qFNF2TidHCe-1)   
Paul Graham Blogs - [paulgraham.com](http://www.paulgraham.com/articles.html)   

How to Get Rich (without getting lucky), by Naval Ravikant - [twitter.com](https://twitter.com/naval/status/1002103360646823936)   
Naval Ravikant Podcast - [spotify.com](https://open.spotify.com/show/7qZAVw03FuurfYnWIWwkHY) | [podcasts.apple.com](https://podcasts.apple.com/us/podcast/naval/id1454097755)    

# Free Learning Resources (Computer-Science & Programming)

Computer Science, Roadmap - [github.com](https://github.com/ossu/computer-science)   
Roadmap for Learning System-level Programming (Assembly & C) - [github.com](https://github.com/gurugio/lowlevelprogramming-university)   
List of Amazing Computer-Science Research Papers - [github.com](https://github.com/papers-we-love/papers-we-love)   

Master Command Line, in 1 Page - [github.com](https://github.com/jlevy/the-art-of-command-line)   

# Free Learning Resources (Physics)

The Theoretical Minimum Lectures, by Leonard Susskind - [theoreticalminimum.com](https://theoreticalminimum.com/courses)   

# Free Learning Resources (Mathematics)

3Blue1Brown, Fall in Love with Math - [youtube.com](https://www.youtube.com/channel/UCYO_jab_esuFRV4b17AJtAw)   
A List of Free Learning Resources - [github.com](https://github.com/rossant/awesome-math)   