Okay, this post includes my life long obsession for writing against "Education System", especially the Indian Education System. As in the title - "Why did I quit?", I am actually going to justify my decision of dropping out of formal education - why did I do so? what did I learn? And why you ~~should~~ or shouldn't consider doing the same (do not take it seriously).

There's rarely going to be anything that you might not already know about, but I'm gonna write about it anyway. Also, I'm not sure why would someone be interested in my story. But hey, here's a note if you are (or maybe not).

Did you notice there's no comment section below? There's definitely a reason behind it - I don't want anyone to justify my decision and throw advice in there. You need to understand that this post has come more from my instincts, my emotions, than reasoning. But, feel free to throw some in my inbox.

# Education and School

<img src="https://gitlab.com/ritwik310/blog-documents/raw/master/Assets/Rick-Quote-On-School-0.jpg" height="250" />

If you want to be average school is the most efficient option, If you want to be smart you need to find other arrangements (parallel or non-parallel). Education is about learning, School is about winning - and that's where school misses out on its very purpose, which is education.

<!--Also, a system that is focused more on the elimination of participants, than the distribution of services is not at all in everyone's best interest.-->

Education is the process of facilitating learning, or the acquisition of knowledge, skills, values, beliefs, and habits. Educational methods include storytelling, discussion, teaching, training, and directed research. Education frequently takes place under the guidance of educators, however, learners may also educate themselves. Education can take place in formal or informal settings and any experience that has a formative effect on the way one thinks, feels, or acts may be considered educational. (Wikipedia)

"Education is about the acquisition of knowledge, skills, values, beliefs, and habits" - there's no word like "competition", "exams" or "passing thresholds" in this definition. Cause those are not at all related to education, it's just a medium of motivation for people who go through the system, meant to educate in mass. Everything that humans do is somehow related to a reward system. Even there's a reward system responsible for our eating. Exams, competition, and status are just rewards that we are introduced to in order to take "study" seriously.

For example, in an age of Google one doesn't need to know "In which year world war II started?". In fact, I just googled this topic while writing this note.

<!--[GIF of Google Search - "Important historical events"]-->
<!--![Alt Text](https://gitlab.com/ritwik310/blog-documents/raw/master/Assets/Google-Search-_Important-History-Events-0.gif)-->
<img src="https://gitlab.com/ritwik310/blog-documents/raw/master/Assets/Google-Search-_Important-History-Events-0.gif" height="250" />

It's not at all important to remember when World War II started - that is not education. But, understanding that World War II happened in somewhere mid 20th century and that lead to a revolution in missiles, nuclear technology, and communication systems - that eventually helped us land a man on the surface of the moon, nuclear powerplants, Internet and more - that's what education looks like. Human brains are not at all designed to store unrelatable data. Investing your energy and time to remember facts that you can write in exams (and probably forget after a while), is just a waste of time, energy and entropy. That's not in the best interests of smart people. (bad example btw - one should know when WW2 started though, it's axiomatic :p)

School system is just an illusion of education. In fact, resist participants from real education by making learning uncool, and rewarding memorization. But one might also argue, that it works fine. And its true everyone who follows the status quo ends up being fine in their lives. The system is designed in a way that it rewards non-creative work too (and I totally get that it's important for a developing country like ours to have). But remember, if the system fails (for example fails to provide enough jobs), you're doomed. A system that is powered by capital can only sustain itself as long as it follows the principles of economics, an imperfect model is not a good choice to put all your eggs into (Not an advice though).

But, schools are important anyways and it was important for me early on. Remember the purpose of this blog is not to convince you to quit school, it's just justifying my decision to drop out. If you want to be a doctor you have to go through Med-School, if you want to have a Ph.D. in physics, you need to go through college programs. The education system is no doubt the biggest contributor to a country's economy, its development and scientific resourcefulness. But that doesn't mean that it's perfect (in fact, it's crap). In my case, It did not meet my needs - the opportunity cost getting a positive outcome from going through formal education was too much for me to put my all energy into. So, the smart choice was to quit and to focus on what I really care about.



